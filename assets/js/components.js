//
class nav extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
   
    <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <!--  <h1 class="logo me-auto"><a href="index.html">RDO</a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
     <a href="index.html" class="logo me-auto"><img src="assets/img/rdo_logo.png" alt="" class="img-fluid"></a>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a class="active" href="index.html">Home</a></li>
          <li><a  href="agenda.html">Agenda</a></li>
          <li><a href="services.html">Services</a></li>
          <li class="dropdown"><a href="productivity.html"><span>Research Productivity</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="#">Research Publications</a></li>
              <li class="dropdown"><a href="#"><span>Research Projects</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Institutional</a></li>
                  <li><a href="#">Externally Funded</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="pricing.html">Faculty Research Involvement</a></li>
          <li><a href="trainers.html">Research Centers</a></li>
          <li><a href="contact.html">Highlights of 2022</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

      

    </div>
  </header><!-- End Header -->

    `;
  }
}
///////////////////////////////////////////////////////////////////////////////
class nav1 extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
   
    <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <!--  <h1 class="logo me-auto"><a href="index.html">RDO</a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
     <a href="index.html" class="logo me-auto"><img src="assets/img/rdo_logo.png" alt="" class="img-fluid"></a>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a  href="index.html">Home</a></li>
          <li><a class="active" href="agenda.html">Agenda</a></li>
          <li><a href="services.html">Services</a></li>
          <li class="dropdown"><a href="productivity.html"><span>Research Productivity</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="#">Research Publications</a></li>
              <li class="dropdown"><a href="#"><span>Research Projects</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Institutional</a></li>
                  <li><a href="#">Externally Funded</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="pricing.html">Faculty Research Involvement</a></li>
          <li><a href="trainers.html">Research Centers</a></li>
          <li><a href="contact.html">Highlights of 2022</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

      

    </div>
  </header><!-- End Header -->

    `;
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
class nav2 extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
   
    <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <!--  <h1 class="logo me-auto"><a href="index.html">RDO</a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
     <a href="index.html" class="logo me-auto"><img src="assets/img/rdo_logo.png" alt="" class="img-fluid"></a>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a  href="index.html">Home</a></li>
          <li><a  href="agenda.html">Agenda</a></li>
          <li><a class="active" href="services.html">Services</a></li>
          <li class="dropdown"><a href="productivity.html"><span>Research Productivity</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="#">Research Publications</a></li>
              <li class="dropdown"><a href="#"><span>Research Projects</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Institutional</a></li>
                  <li><a href="#">Externally Funded</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="pricing.html">Faculty Research Involvement</a></li>
          <li><a href="trainers.html">Research Centers</a></li>
          <li><a href="contact.html">Highlights of 2022</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

      

    </div>
  </header><!-- End Header -->

    `;
  }
}

class nav3 extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
   
    <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <!--  <h1 class="logo me-auto"><a href="index.html">RDO</a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
     <a href="index.html" class="logo me-auto"><img src="assets/img/rdo_logo.png" alt="" class="img-fluid"></a>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a  href="index.html">Home</a></li>
          <li><a  href="agenda.html">Agenda</a></li>
          <li><a  href="services.html">Services</a></li>
          <li class="dropdown"><a class="active" href="productivity.html"><span>Research Productivity</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="#">Research Publications</a></li>
              <li class="dropdown"><a href="#"><span>Research Projects</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Institutional</a></li>
                  <li><a href="#">Externally Funded</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="pricing.html">Faculty Research Involvement</a></li>
          <li><a href="trainers.html">Research Centers</a></li>
          <li><a href="contact.html">Highlights of 2022</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

      

    </div>
  </header><!-- End Header -->

    `;
  }
}
customElements.define('main-nav', nav);
customElements.define('main-nav1', nav1);
customElements.define('main-nav2', nav2);
customElements.define('main-nav3', nav3);

